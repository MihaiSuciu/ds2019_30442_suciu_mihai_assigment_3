﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3Client.Models
{
    public class Medication
    {
        public int MedicationID { get; set; }
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public int Dosage { get; set; }
        public int MedicationPlan_MedicationPlanID { get; set; }
    }
}
