﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GrpcGreeter.Services
{
    public class GetPlanService
    {
        public string GetAllEventData() //Get All Events Records  
        {
            string result;
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                result = client.DownloadString("http://localhost:44341/api/MedicationPlan"); //URI  
                Console.WriteLine(Environment.NewLine + result);
            }
            return result;
        }
    }
}
