using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment3Client.Models;
using Grpc.Core;
using GrpcGreeter.Services;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace GrpcGreeter
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<MedicationPlanMessage> SayMedication(MedicationPlanMessage request, ServerCallContext context)
        { 
            
            GetPlanService planService = new GetPlanService();
            //json from assignment 1 - MedicationPlans
            var json = planService.GetAllEventData();
            var medicationPlan = JsonConvert.DeserializeObject<MedicationPlan>(json.Substring(1, json.Length - 2));

            //Here I will have all medications
            var medicationMessage = new List<MedicationMessage>();
            foreach(var medication in medicationPlan.Medications)
            {
                medicationMessage.Add(new MedicationMessage
                {
                    MedicationID=medication.MedicationID,
                    Dosage=medication.Dosage,
                    Name=medication.Name,
                    SideEffects=medication.SideEffects
                });
            }
            
            //Here I create the MedicationPlanMessage
            var message = new MedicationPlanMessage
            {
                MedicationPlanID = medicationPlan.MedicationPlanID,
                Days = medicationPlan.Days,
                Description = medicationPlan.Description
                //Medications = medicationMessage
            };

            //Adding in MedicationPlanMessage the medications.
            message.Medications.AddRange(medicationMessage);

            return Task.FromResult(message);

        }
    }
}
