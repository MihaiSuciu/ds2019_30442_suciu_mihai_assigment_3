﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3Client.Models
{
    public class MedicationPlan
    {
        public int MedicationPlanID { get; set; }
        public int Days { get; set; }
        public string Description { get; set; }
        public List<Medication> Medications { get; set; }
    }
}
