﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3Client.Models
{
    public class Patient
    {
        public int PatientID { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string Address { get; set; }
    }
}
