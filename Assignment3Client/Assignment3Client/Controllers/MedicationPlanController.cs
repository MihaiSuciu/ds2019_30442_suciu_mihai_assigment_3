﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment3Client.Models;
using Grpc.Net.Client;
using GrpcGreeter;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Assignment3Client.Controllers
{
    public class MedicationPlanController : Controller
    {
        public MedicationPlanController()
        {

        }
        public async Task<IActionResult> Index()
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
            //Here I take the MedicationPlanMessage from the server
            var reply = await client.SayMedicationAsync(new MedicationPlanMessage());
            
            //Create the medication list
            var medications = new List<Medication>();
            foreach(var medication in reply.Medications)
            {
                medications.Add(new Medication
                {
                    MedicationID=medication.MedicationID,
                    Dosage=medication.Dosage,
                    Name=medication.Name,
                    SideEffects=medication.SideEffects
                });
            }

            //Creating the medication plan
            MedicationPlan medicationPlan = new MedicationPlan
            {
                MedicationPlanID = reply.MedicationPlanID,
                Description = reply.Description,
                Days = reply.Days,
                Medications=medications
            };
            ViewBag.Message = medicationPlan;
            return View();
        }
    }
}