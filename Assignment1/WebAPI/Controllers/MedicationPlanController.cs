﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class MedicationPlanController : ApiController
    {
        private HospitalEntities1 db = new HospitalEntities1();

        // GET: api/MedicationPlan
        public IQueryable<MedicationPlan> GetMedicationPlans()
        {
            return db.MedicationPlans;
        }

        // GET: api/MedicationPlan/5
        [ResponseType(typeof(MedicationPlan))]
        public IHttpActionResult GetMedicationPlan(int id)
        {
            MedicationPlan medicationPlan = db.MedicationPlans.Find(id);
            if (medicationPlan == null)
            {
                return NotFound();
            }

            return Ok(medicationPlan);
        }

        // PUT: api/MedicationPlan/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMedicationPlan(int id, MedicationPlan medicationPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicationPlan.MedicationPlanID)
            {
                return BadRequest();
            }

            db.Entry(medicationPlan).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicationPlanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MedicationPlan
        [ResponseType(typeof(MedicationPlan))]
        public IHttpActionResult PostMedicationPlan(MedicationPlan medicationPlan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MedicationPlans.Add(medicationPlan);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = medicationPlan.MedicationPlanID }, medicationPlan);
        }

        // DELETE: api/MedicationPlan/5
        [ResponseType(typeof(MedicationPlan))]
        public IHttpActionResult DeleteMedicationPlan(int id)
        {
            MedicationPlan medicationPlan = db.MedicationPlans.Find(id);
            if (medicationPlan == null)
            {
                return NotFound();
            }

            db.MedicationPlans.Remove(medicationPlan);
            db.SaveChanges();

            return Ok(medicationPlan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MedicationPlanExists(int id)
        {
            return db.MedicationPlans.Count(e => e.MedicationPlanID == id) > 0;
        }
    }
}