﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;

namespace Consumer
{
    class Program
    {
        public static List<Activity> GetProblems(List<Activity> activities)
        {
            List<Activity> problemsActivity = new List<Activity>();
            int count = 0;
            for (int i = 0; i < activities.Count; i++)
            {
                if (activities[i].ActivityLabel.Equals("Sleeping") && (activities[i].EndTime - activities[i].StartTime).TotalHours > 12)
                {
                    System.Console.WriteLine("Activity " + i + " where activity is: " + activities[i].ActivityLabel + " and is for " + (activities[i].EndTime - activities[i].StartTime).TotalHours.ToString("n2") + " hours"); ;
                    problemsActivity.Add(activities[i]);
                    count++;
                }
                if (activities[i].ActivityLabel.Equals("Leaving") && (activities[i].EndTime - activities[i].StartTime).TotalHours > 12)
                {
                    System.Console.WriteLine("Activity " + i + " where activity is: " + activities[i].ActivityLabel + " and is for " + (activities[i].EndTime - activities[i].StartTime).TotalHours.ToString("n2") + " hours"); ;
                    problemsActivity.Add(activities[i]);
                    count++;
                }
                if (activities[i].ActivityLabel.Equals("Toileting") && (activities[i].EndTime - activities[i].StartTime).TotalHours > 1)
                {
                    System.Console.WriteLine("Activity " + i + " where activity is: " + activities[i].ActivityLabel + " and is for " + (activities[i].EndTime - activities[i].StartTime).TotalHours.ToString("n2") + " hours"); ;
                    problemsActivity.Add(activities[i]);
                    count++;
                }
            }
            return problemsActivity;
        }
        static void Main(string[] args)
        {
            List<Activity> activities = new List<Activity>();

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    System.Console.WriteLine(" [x] Received {0}", message);
                    Activity activity = new Activity(message);
                    System.Console.WriteLine(activity.ActivityLabel + (activity.EndTime - activity.StartTime).TotalHours);

                    activities.Add(activity);

                };
                channel.BasicConsume(queue: "hello",
                                     autoAck: true,
                                     consumer: consumer);
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
            GetProblems(activities);

        }
    }
}
