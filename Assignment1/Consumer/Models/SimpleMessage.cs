﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Consumer.Models
{
    class SimpleMessage
    {

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("body")]
        public dynamic Body { get; set; }

        [JsonProperty("sessionId")]
        public Guid SessionId { get; set; }

    }
}
