﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Models
{
    public class MedicationPlan
    {
        public int MedicationPlanID { get; set; }
        public int Days { get; set; }
        public string Description { get; set; }
        public ICollection<Medication> Medications { get; set; }

    }
}