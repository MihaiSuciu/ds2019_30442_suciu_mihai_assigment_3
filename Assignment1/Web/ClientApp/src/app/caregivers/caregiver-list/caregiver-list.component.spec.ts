﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { CaregiverListComponent } from './caregiver-list.component';

let component: CaregiverListComponent;
let fixture: ComponentFixture<CaregiverListComponent>;

describe('caregiver-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CaregiverListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(CaregiverListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});